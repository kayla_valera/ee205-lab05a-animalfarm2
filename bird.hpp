///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Kayla Valera  <kvalera@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 28_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "animal.hpp"

namespace animalfarm {

	   class Bird : public Animal {
		         public: 
	enum Color featherColor;
	string 		isMigratory;

	void printInfo();
	
	virtual const string speak(); 
 };
} // namespace animalfarm

