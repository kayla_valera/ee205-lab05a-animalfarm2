///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @todo Kayla Valera <@todo kvalera@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @todo 27_FEB_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>
#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float newWeight, enum Color newColor, enum Gender newGender ) {
		gender = newGender;
		
		weight = newWeight;

		species = "Katsuwonus pelamis";

		scaleColor = newColor; 

		favoriteTemp = 75;


	}


	void Aku::printInfo() {
		cout << "Aku" << endl;
		cout << "   Weight = [" << weight << "]" << endl;
		Fish::printInfo();


	}

}

